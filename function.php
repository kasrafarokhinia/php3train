<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
const BASE_URL = "http://localhost/7learn/php3train/";
const USERS_DB = "/Applications/MAMP/htdocs/7learn/php3train/db/users.json";
const LOGIN_PAGE = "http://localhost/7learn/php3train/auth/login.php";
const register_PAGE = "http://localhost/7learn/php3train/auth/register.php";

function siteUrl($url = '')
{

    echo 'http://localhost/7learn/php3train/' . $url;
}

function getUsers($return_assoc = 0)
{
    $users  = json_decode(file_get_contents(USERS_DB), $return_assoc);
    return (array) $users;
}
function saveUsers(stdClass $user): bool
{
    $users  = getUsers(1);
    $users[] = (array) $user;
    $users_json = json_encode($users);
    file_put_contents(USERS_DB, $users_json);
    return true;
}

function isAdmin($user_name = 0)
{

    return 1;
}
function logout()
{
    session_destroy();
    jsRedirect($_SERVER['HTTP_REFERER'], 0.1);
}

function isloggedIn()
{

    return isset($_SESSION['login']);
}
function login($email, $password)
{
    $users = getUsers();

    foreach ($users as $user) {

        if ($user->password == $password  && $user->email == $email) {
            $_SESSION['login'] = $user->email;
            return true;
        }
    }
    return false;
}



function jsRedirect($url, $secs)
{
    echo "
    <script>
         setTimeout(function(){
            window.location.href = '" . $url . "';
         }, " . $secs * 1000 . ");
      </script>";
}
