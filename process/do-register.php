<?php


include_once '../function.php';


if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $userFields = array(
        'username' => $_POST['username'],
        'email' => $_POST['email'],
        'password' => $_POST['password']
    );
    $username = trim($_POST['username']);
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);

    if (login($email, $password)) {
        echo "The email address you have entered is already registered!<br>";
        echo "Redirecting to Login Page ...";
        jsRedirect(LOGIN_PAGE, 3);
    } else if ($email !== null && $password !== null){
        saveUsers((object) $userFields);
        echo "Login Successfull!<br>";
        echo "Redirecting to Login Page ...";
        jsRedirect(LOGIN_PAGE, 3);
    }else{

        echo'Information is not true try again!';
        echo'</br>';
        echo "Redirecting to register Page ...";
        jsRedirect(register_PAGE, 13);
    }


}
