<?php



include '../function.php';
if (isset($_GET['action']) && $_GET['action'] == 'logout') {
    logout();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Register Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="../assets/css/style.css">
</head>

<body>
    <!-- partial:index.partial.html -->
    <div class="login">
        <form action="<?php siteUrl('process/do-register.php') ?>" method="POST">

            <h1>Register Form</h1>
            <!-- <label for="username">Username:</label> -->
            <input type="text" id="username" name="username" placeholder="username" required>
            <input type="text" id="email" name="email" placeholder="Email" required>
            <!-- <label for="password">Password:</label> -->
            <input type="password" id="password" name="password" placeholder="Password" required>
            <button type="submit">Login</button>
        </form>

    </div>
    <!-- partial -->

</body>

</html>